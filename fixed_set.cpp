﻿#include <iostream>
#include <vector>
#include <random>


/* Линейная хэш функция */
struct linear_hash_function {
private:
    /* h(x) =  ((factor * x + summand) % prime) */
    unsigned long long prime;
    unsigned long long factor;
    unsigned long long summand;

public:
    /* prime по умолчанию */
    static const unsigned long long DEFAULT_PRIME = 2000000011;

    /* Инициализирует factor, summand и prime */
    linear_hash_function (unsigned int factor,
        unsigned int summand,
        unsigned int prime = DEFAULT_PRIME) :
        prime(prime),
        factor(factor),
        summand(summand) {
    }

    linear_hash_function () {
    }

    /* Вычисляет хэш функцию по ключу */
    int operator() (unsigned long long key) const {
        return (factor * key + summand) % prime;
    }
};






    
/* Универсальное семейство линейных хэш функций. Содержит
число prime и умеет выдавать случайные функции с параметром prime */
class linear_hash_function_generator {
private:
    std::default_random_engine randomizer;
    unsigned long long prime;

    /* seed по умолчанию */
    static const int DEFAULT_SEED = 1;

public:
    /* Инициализирует семейство с prime = linear_hash_function::DEFAULT_PRIME 
    и рандомизатор с seed = DEFAULT_SEED */
    linear_hash_function_generator () :
        randomizer(DEFAULT_SEED),
        prime(linear_hash_function::DEFAULT_PRIME) {
    }

    /* Инициализирует семейство с prime и seed */
    linear_hash_function_generator (unsigned long long prime, int seed = DEFAULT_SEED) :
        randomizer(seed),
        prime(prime) {
    }

    /* Устанавливает prime */
    void set_prime (unsigned long long new_prime) {
        prime = new_prime;
    }

    /* Устанавливает рандомизатору seed */
    void set_seed (int seed) {
        randomizer.seed(seed);
    }

    typedef linear_hash_function hash_function_type;

    /* Возвращает функцию из универсального семейства с параметром семейства prime 
    и случайными factor из [1, prime) и summand из [0, prime) */
    linear_hash_function generate () {
        return linear_hash_function(randomizer() % (prime - 1) + 1, randomizer() % prime, prime);
    }
};






/* fixed_set */
template <class T, class universal_family_generator>
class fixed_set {
private:
    /* Константа c по умолчанию, c > 2 */
    static unsigned const int DEFAULT_MULTIPLIER = 5;

    typedef typename universal_family_generator::hash_function_type hash_function_type;

    /* Семейство, из которого мы черпаем хэш функции */
    universal_family_generator hash_functions_family;

    /* Набор заданных ключей */
    std::vector<T> initial_set;

    /* Первая хэш функция */
    hash_function_type first_hash_function;

    /* Вторая хэш функция */
    hash_function_type second_hash_function;

    /* Разметка графа - числа в вершинах, которые мы
    проставляем */
    std::vector<int> node_marking;

    /* Число вершин m = cn, c > 2 */
    int nodes_number;
    

    /* Класс умеет по заданным хэш функциям, набору ключей и заданному числу вершин
    (m = cn, c > 2) строить граф и определять, можно ли расставить числа в вершинах.
    Если можно, то класс умеет возвращать эту расстановку в массиве */
    class node_marker {
    private:
        /* Ребро графа */
        struct edge {
            /* Вес ребра */
            int weight;

            /* Номер вершины, в которую ведет ребро */
            int node_to;

            /* Инициализирует ребро заданными значениями */
            edge (int weight, int node_to) :
                weight(weight),
                node_to(node_to) {
            }
        };

        typedef std::vector<std::vector<edge> > graph_type;

        /* Число ребер */
        int edges_number;

        /* Масиив флагов посещенных вершин */
        std::vector<char> node_visited;

        /* Граф */
        graph_type graph;

        /* Разметка графа */
        std::vector<int> node_marking;

        /* Удалось ли разметить граф в конструкторе */
        bool marked;


        /* По набору ключей set, и двум хэш функциям строит граф
        и возвращет его */
        graph_type make_graph (int nodes_number,
            const std::vector<T> & initial_set,
            const hash_function_type & first_hash_function,
            const hash_function_type & second_hash_function) const {

            graph_type new_graph(nodes_number);

            for (int i = 0; i < initial_set.size(); ++i) {
                int first_node = first_hash_function(initial_set[i]) % nodes_number;
                int second_node = second_hash_function(initial_set[i]) % nodes_number;

                new_graph[first_node].push_back(edge(i, second_node) );
                new_graph[second_node].push_back(edge(i, first_node) );
            }
            return new_graph;
        }


        /* Приписывает значение value вершине с номером node_number в графе graph.
        (меняет node_marking) и вызывает себя от соседней вершины, и т.д. 
        Возвращает true, если удалось расставить числа в данной связной компоненте,
        false - иначе */
        bool recursively_set_value (int node_number, int value) {
            node_marking[node_number] = value;
            node_visited[node_number] = true;

            for (int i = 0; i < graph[node_number].size(); ++i) {
                const edge current_edge(graph[node_number][i]);
                int next_value = (current_edge.weight - value + edges_number) 
                    % edges_number;
                if (!node_visited[current_edge.node_to]) {
                    if (!recursively_set_value(current_edge.node_to, next_value)) {
                        return false;
                    }
                } else if (node_marking[current_edge.node_to] != next_value) {
                    return false;
                }
            }

            return true;
        }


        /* Пытаеться расставить числа в вершинах графа graph (меняет node_marking).
        Возвращает true, если удалось, false - иначе */
        bool try_to_mark_nodes () {
            for (int i = 0; i < graph.size(); ++i) {
                if (!node_visited[i]) {
                    if (!recursively_set_value(i, 0)) {
                        return false;
                    }
                }
            }
            return true;
        }


    public:
        node_marker (const hash_function_type & first_hash_function,
            const hash_function_type & second_hash_function,
            const std::vector<T> & initial_set,
            int nodes_number) :
            edges_number(initial_set.size() ),
            node_visited(nodes_number, false),
            graph(make_graph(nodes_number, initial_set,
                first_hash_function, second_hash_function) ),
            node_marking(nodes_number, 0),
            marked(try_to_mark_nodes() ) {
        }

        /* Возвращает true, если в конструкторе удалось разметить граф. false - иначе */
        bool done () const {
            return marked;
        }


        /* Возвращает node_marking */
        std::vector<int> get_marking () const {
            return node_marking;
        }
    };


    /* Подбирает подходящие хэш функции и разметку */
    void make_node_marking ()  {
        bool done;
        do {
            first_hash_function = hash_functions_family.generate();
            second_hash_function = hash_functions_family.generate();
            node_marker builder
                (first_hash_function, second_hash_function, initial_set, nodes_number);

            done = builder.done();

            if (done) {
                node_marking = builder.get_marking();
            }
        } while (!done);
    }




public:
    /* Конструктор, создающий fised_set с set = numbers и параметром c = multiplier */
    fixed_set (const std::vector<T> & numbers, const universal_family_generator 
        & hash_functions_family, int multiplier = DEFAULT_MULTIPLIER) :
        hash_functions_family(hash_functions_family),
        initial_set(numbers),
        node_marking(multiplier * numbers.size(), 0),
        nodes_number(multiplier * numbers.size()) {
        make_node_marking ();
    }



    /* Возвращает true, если number есть в set. false - иначе */
    bool contains (const T & number) const {
        const int first_label = node_marking[first_hash_function(number) % nodes_number];
        const int second_label = node_marking[second_hash_function(number) % nodes_number];

        const int possible_position = (first_label + second_label) % initial_set.size();

        return (number == initial_set[possible_position]);
    }
};











/* Считывает числа */
std::vector<int> get_numbers (std::istream & input_stream = std::cin) {
    int size;
    input_stream >> size;

    std::vector<int> numbers(size);

    for (int i = 0; i < numbers.size(); ++i) {
        input_stream >> numbers[i];
    }

    return numbers;
}







int main() {
    std::ios_base::sync_with_stdio(false);

    std::cin.tie(NULL);

    std::cout.tie(NULL);

    const std::vector<int> numbers(get_numbers() );

    const fixed_set<int, linear_hash_function_generator> my_set(numbers, 
        linear_hash_function_generator() );

    int number_of_requests;

    std::cin >> number_of_requests;

    for (int i = 0; i < number_of_requests; ++i) {
        int request;
        std::cin >> request;
        if (my_set.contains(request)) {
            std::cout << "Yes\n";
        } else {
            std::cout << "No\n";
        }
    }

    return 0;
}


